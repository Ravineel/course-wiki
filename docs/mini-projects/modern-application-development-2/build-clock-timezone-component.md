# Friend's Time - Build a clock with timezone component


## Problem
- Your friends or collegues live and work in different timezones
- You want a single page app that list thier names and time in their timezone

<object data="../friends-timezone.svg" width="90%"></object>

## Solution Ideas
- Build a vue component - call it `<friend-time>`
- Inputs for the component will be name and timezone, call it `<friend-time>`
- You can add or remove as many `friend-time` to page
- Build a simple single page app
- You can store the current state of the app in localstorage

